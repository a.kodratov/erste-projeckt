-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 05. Apr 2024 um 11:00
-- Server-Version: 10.4.28-MariaDB
-- PHP-Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ff`
--
CREATE DATABASE IF NOT EXISTS `ff` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ff`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fllight`
--

DROP TABLE IF EXISTS `fllight`;
CREATE TABLE `fllight` (
  `pid` int(10) UNSIGNED NOT NULL,
  `preis` decimal(10,2) DEFAULT NULL,
  `menge` int(10) UNSIGNED DEFAULT NULL,
  `titel` varchar(40) DEFAULT NULL,
  `bild` varchar(50) DEFAULT NULL,
  `link` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `fllight`
--

INSERT INTO `fllight` (`pid`, `preis`, `menge`, `titel`, `bild`, `link`) VALUES
(1, 100000.00, 1, 'B737', 'b737.jpg', 'html/b737.html'),
(2, 250000.00, 1, 'B777', 'b777.jpg', 'html/b777.html'),
(3, 300000.00, 1, 'B747', 'b747.jpg', 'html/b747.html'),
(4, 500000.00, 1, 'An124', 'an124.jpg', 'html/an124.html'),
(5, 550000.00, 1, 'C5', 'c5.jpg', 'html/c5.html'),
(6, 350000.00, 1, 'Il76', 'Il76.jpg', 'html/il76.html');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunden`
--

DROP TABLE IF EXISTS `kunden`;
CREATE TABLE `kunden` (
  `id` int(11) NOT NULL,
  `Kontaktname` varchar(30) DEFAULT NULL,
  `Kontakts` varchar(30) DEFAULT NULL,
  `Data` varchar(30) DEFAULT NULL,
  `Route` varchar(50) DEFAULT NULL,
  `Aircraft_name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `kunden`
--

INSERT INTO `kunden` (`id`, `Kontaktname`, `Kontakts`, `Data`, `Route`, `Aircraft_name`) VALUES
(1, 'Roy Jons', '49456789876', '23 September 2024', 'EMA-LEJ', 'B737'),
(2, 'Anton Kodratov', '49456789895', '12 May 2024', 'HHN-MEX', 'B737'),
(3, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL),
(5, 'Sergey', '9878987', '25May 2024', 'from HHN to EMA', 'B737'),
(6, 'Stephan', 'Fielitz', '04 Feb 2025', 'LEJ - DWC', 'B737'),
(7, 'Anton', 'Ten', '04 Feb 2025', 'from HHN to IHA', 'B737'),
(8, 'Annet Helmich', '+777777777', '23May 2024', 'from HHN to EMA', 'B737'),
(9, 'Leonardo', '9999999999', '01Junly 2025', 'IAH - MAL', 'B737'),
(10, 'Leonardo', '9999999999', '01Junly 2025', 'IAH - MAL', 'B737'),
(11, 'Stephan', 'Fielitz', '25May 2024', 'from HHN to EMA', 'B737'),
(12, 'Sergey', '9878987', '25May 2024', 'from HHN to IHA', 'B737'),
(13, 'Anton', 'Ten', '04 Feb 2025', 'from HHN to IHA', 'B737'),
(14, 'Anton', 'Fielitz', '25May 2024', 'from HHN to IHA', 'B737'),
(15, 'test', 'test', '05042024', 'LEj LEJ', 'B737');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `fllight`
--
ALTER TABLE `fllight`
  ADD PRIMARY KEY (`pid`);

--
-- Indizes für die Tabelle `kunden`
--
ALTER TABLE `kunden`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `fllight`
--
ALTER TABLE `fllight`
  MODIFY `pid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `kunden`
--
ALTER TABLE `kunden`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
