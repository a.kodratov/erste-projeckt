<h1> Ihre Eingabe wurde beendet</h1>
<br/>
<a href="../index.php">Zurück zum Flugzeugs Auswähl</a>
<br/>
<br/>
<hr>
<p>Wir rufen sie nach 30minuten an.</p>

<?php


// Anmelden am Server
$conny = new mysqli("localhost", "root", "", "ff");


// wir starten eine Transaktion, falla noch andere Clients gleichzeitig
//auf Server zugreifen

$conny -> query("set session transaction isolation level read committed");
$conny ->begin_transaction();    // in SQL : set autocommit = 0;


// Variabllen für die Speicherung der Eingaben

$kontaktname = $_REQUEST["kontaktname"];
$kontakts = $_REQUEST["kontakts"];
$data = $_REQUEST["data"];
$route = $_REQUEST["route"];
$aircraft_name = $_REQUEST["aircraft_name"];

//SQL erstellen
//für die Werte, die eingetragen werden, verwenden wir Platzhalter ?
//das ist ein PreparedStatment (kann man so nicht machen)
//verhindert SQL Injection

$sql = "insert into kunden (kontaktname,kontakts,data,route,aircraft_name ) values (?,?,?,?,?)";

//Server soll zu jedem ? dem passenden Datentyp definieren

$insert = $conny->prepare($sql);

//wir legen fest, welche Variablen für die? verwendet werden sollen
// und wir geben an, welchen Datentyp die Variablen haben
// s - string, d -decimal, i =int

$insert -> bind_param("sssss", $kontaktname,$kontakts,$data,$route,$aircraft_name);

//Ausführen der SQL

$insert -> execute();


//Ausgabe: wie viele Zeilen sind betroffen (...rows affected)
print "Anzahl der betroffenen Zeilen";
print $conny->affected_rows;

//Transaction beenden
$conny->commit();  // in SQL : set autocommit = 1;

?>